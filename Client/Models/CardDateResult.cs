﻿using System;

namespace ClientComfy.Models
{
    class CardDateResult
    {
        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime EndCardDate { get; set; }

        public double Balance { get; set; }

        public string CardNumber { get; set; }
    }
}
