﻿namespace ClientComfy.Models
{
    class ChangeBalanceCardDto
    {
        public double ChangeSumm { get; set; }

        public string CardNumber { get; set; }
    }
}
