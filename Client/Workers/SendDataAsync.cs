﻿using ClientComfy.Extension;
using ClientComfy.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClientComfy.Workers
{
    class SendDataAsync
    {
        private const string requestUrl = "https://localhost:44396";
        public async Task<CardDateResult> CreateCardAsync(CardDataDto cardDataDto)
        {
            var responseContent = await SendMultipartRequestAsync(cardDataDto, requestUrl+ "/api/CardData/AddCard");
            var returnData = JsonConvert.DeserializeObject<CardDateResult>(responseContent);
            return returnData;
        }

        public async Task<CardDateResult> FindCardData(FindCardDataDto cardDataDto)
        {
            var responseContent = await SendMultipartRequestAsync(cardDataDto, requestUrl + "/api/CardData/SearchCard");
            var returnData = JsonConvert.DeserializeObject<CardDateResult>(responseContent);
            return returnData;
        }

        public async Task<CardDateResult> ChangeBalance(ChangeBalanceCardDto changeBalanceDto)
        {
            var responseContent = await SendMultipartRequestAsync(changeBalanceDto, requestUrl + "/api/CardData/ChangeBalance");
            var returnData = JsonConvert.DeserializeObject<CardDateResult>(responseContent);
            return returnData;
        }

        private async Task<string> SendMultipartRequestAsync<T>(T form, string url)
        {
            using (var client = new HttpClient())
            {
                var content = new MultipartFormDataContentParser<T>(form).Parse();
                using (
                    var message =
                        await client.PostAsync(url, content))
                {
                    return await message.Content.ReadAsStringAsync();
                }
            }
        }
    }
}
