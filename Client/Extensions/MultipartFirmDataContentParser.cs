﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;

namespace ClientComfy.Extension
{
    public class MultipartFormDataContentParser<T>
    {
        private T _obj;
        public MultipartFormDataContentParser(T obj)
        {
            _obj = obj;
        }
        public MultipartFormDataContent Parse()
        {
            var formContent = new MultipartFormDataContent();
            var genericFileName = "Filename";

            var propertyInfos = typeof(T).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                var value = propertyInfo.GetValue(_obj);
                if (value is byte[] ||
                    value is Stream ||
                    value is List<Stream> ||
                    value is List<byte[]>)
                {
                    if (value is List<Stream> listStream)
                    {
                        foreach (var file in listStream)
                            if (file != null)
                                formContent.Add(new StreamContent(file), propertyInfo.Name, genericFileName);
                    }
                    else if (value is List<byte[]> listByteArray)
                    {
                        foreach (var file in listByteArray)
                            if (file != null)
                                formContent.Add(new ByteArrayContent(file), propertyInfo.Name, genericFileName);
                    }
                    else
                        formContent.Add(getFileContent(value), propertyInfo.Name, genericFileName);
                }
                else if (value != null)
                {
                    string output = value.ToString();
                    if (!String.IsNullOrEmpty(output))
                    {
                        formContent.Add(new StringContent(output), propertyInfo.Name);
                    }
                }
            }
            return formContent;
        }
        private HttpContent getFileContent(object obj)
        {
            if (obj is Stream stream)
            {
                return new StreamContent(stream);
            }
            else if (obj is byte[] byteArray)
            {
                return new ByteArrayContent(byteArray);
            }
            else
            {
                throw new Exception("Type not valid to create a HttpContent of file");
            }
        }
    }

}