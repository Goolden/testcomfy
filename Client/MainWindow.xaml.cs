﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ClientComfy.Models;

namespace ClientComfy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void NumberValidationTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void DecimalTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            bool approvedDecimalPoint = false;

            if (e.Text == ",")
            {
                if (!((TextBox)sender).Text.Contains(","))
                    approvedDecimalPoint = true;
            }

            if (!(char.IsDigit(e.Text, e.Text.Length - 1) || approvedDecimalPoint))
                e.Handled = true;
        }

        private async void CreateCardButton_ClickAsync(object sender, RoutedEventArgs e)
        {
            try {
                if (PhoneNumberTextBox.Text.Length != 10)
                    return;

                    var cardDataDto = new CardDataDto
                {
                    Balance = Convert.ToDouble(BalanceTextBox.Text),
                    FullName = FullNameTextBox.Text,
                    PhoneNumber = PhoneNumberTextBox.Text,
                    EndCardDate = EndDateDatePicker.SelectedDate.Value
                };
                var dataSender = new Workers.SendDataAsync();
                var result = await dataSender.CreateCardAsync(cardDataDto);

                if(result != null)
                {
                    FullnameText.Content = result.FullName;
                    PhoneNumber.Content = result.PhoneNumber;
                    CardEndTimeText.Content = result.EndCardDate.ToString();
                    BalanceText.Content = Convert.ToDouble(result.Balance);
                    CardNumber.Content = result.CardNumber;
                }
                else
                    MessageBox.Show("Uncorrect Data");

            }
            catch (Exception)
            {
                MessageBox.Show("Uncorrect Data");
            }
        }

        private async void SearchCardButton_ClickAsync(object sender, RoutedEventArgs e)
        {
            try
            {

                var findCardDto = new FindCardDataDto
                {
                    Data = CardOrPhoneTextBox.Text
                };

                var dataSender = new Workers.SendDataAsync();
                var result = await dataSender.FindCardData(findCardDto);

                if (result != null)
                {
                    FullnameText.Content = result.FullName;
                    PhoneNumber.Content = result.PhoneNumber;
                    CardEndTimeText.Content = result.EndCardDate.ToString();
                    BalanceText.Content = Convert.ToDouble(result.Balance);
                    CardNumber.Content = result.CardNumber;
                }
                else
                    MessageBox.Show("Uncorrect Data");
            }
            catch (Exception)
            {
                MessageBox.Show("Uncorrect Data");
            }
        }

        private async void WithdrawButton_ClickAsync(object sender, RoutedEventArgs e)
        {
            try
            {
                var changeBalanceCard = new ChangeBalanceCardDto
                {
                    ChangeSumm = Convert.ToDouble(WithdrawFromCard_TextBox.Text) * -1,
                    CardNumber = CardNumber.Content.ToString()
                };

                var dataSender = new Workers.SendDataAsync();
                var result = await dataSender.ChangeBalance(changeBalanceCard);

                if (result != null)
                {
                    FullnameText.Content = result.FullName;
                    PhoneNumber.Content = result.PhoneNumber;
                    CardEndTimeText.Content = result.EndCardDate.ToString();
                    BalanceText.Content = Convert.ToDouble(result.Balance);
                    CardNumber.Content = result.CardNumber;
                }
                else
                    MessageBox.Show("Uncorrect Data");
            }
            catch (Exception)
            {

                MessageBox.Show("Uncorrect Data");
            }
            
        }

        private async void AddBalanceButton_ClickAsync(object sender, RoutedEventArgs e)
        {
            try
            {
                var changeBalanceCard = new ChangeBalanceCardDto
                {
                    ChangeSumm = Convert.ToDouble(AddBalanceTextBox.Text),
                    CardNumber = CardNumber.Content.ToString()
                };

                var dataSender = new Workers.SendDataAsync();
                var result = await dataSender.ChangeBalance(changeBalanceCard);

                if (result != null)
                {
                    FullnameText.Content = result.FullName;
                    PhoneNumber.Content = result.PhoneNumber;
                    CardEndTimeText.Content = result.EndCardDate.ToString();
                    BalanceText.Content = Convert.ToDouble(result.Balance);
                    CardNumber.Content = result.CardNumber;
                }
                else
                    MessageBox.Show("Uncorrect Data");
            }
            catch (Exception)
            {
                MessageBox.Show("Uncorrect Data");
            }
        }
    }
}
