﻿using System;

namespace ServerComfy.Models
{
    public class CardDataDto
    {
        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime EndCardDate { get; set; }

        public double Balance { get; set; }
    }
}
