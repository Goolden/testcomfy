﻿using System;

namespace ServerComfy.Models
{
    public class CardDataResult
    {
        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime EndCardDate { get; set; }

        public double Balance { get; set; }

        public string CardNumber { get; set; }
    }
}
