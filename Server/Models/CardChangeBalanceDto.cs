﻿namespace ServerComfy.Models
{
    public class CardChangeBalanceDto
    {
        public double ChangeSumm { get; set; }

        public string CardNumber { get; set; }
    }
}
