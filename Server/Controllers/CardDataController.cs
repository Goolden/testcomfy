﻿using Microsoft.AspNetCore.Mvc;
using ServerComfy.Abstrations;
using ServerComfy.Models;
using System.Threading.Tasks;

namespace ServerComfy.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardDataController : Controller
    {
        private readonly ICardDataService cardDataService;

        public CardDataController([FromServices] ICardDataService cardDataService)
        {
            this.cardDataService = cardDataService;
        }

        [HttpPost("AddCard")]
        public async Task<IActionResult> AddCard([FromForm] CardDataDto cardDataDto)
        {
            var result = await cardDataService.CreateUser(cardDataDto);
            return Ok(result);
        }

        [HttpPost("SearchCard")]
        public async Task<IActionResult> SearchCard([FromForm] CardFindDto cardFindDto)
        {
            var result = await cardDataService.FindCardData(cardFindDto);
            return Ok(result);
        }

        [HttpPost("ChangeBalance")]
        public async Task<IActionResult> ChangeBalance([FromForm] CardChangeBalanceDto cardChangeBalanceDto)
        {
            var result = await cardDataService.ChangeCardBalance(cardChangeBalanceDto);
            return Ok(result);
        }
    }
}
