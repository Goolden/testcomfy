﻿using ServerComfy.Models;
using System.Threading.Tasks;

namespace ServerComfy.Abstrations
{
    public interface ICardDataService
    {
        public Task<CardDataResult> CreateUser(CardDataDto cardDataDto);

        public Task<CardDataResult> FindCardData(CardFindDto cardFindDto);

        public Task<CardDataResult> ChangeCardBalance(CardChangeBalanceDto cardChangeBalanceDto);
    }
}
