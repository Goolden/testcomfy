using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ServerComfy
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var configuration = GetConfiguration();
            try
            {
                var host = BuildWebHost(configuration, args);

                host.Run();

                return 0;
            }
            catch (Exception e)
            {
                var a = e.ToString();
                return 1;
            }
        }

        private static IHost BuildWebHost(IConfiguration configuration, string[] args) =>
           Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webHost =>
           {
               webHost.CaptureStartupErrors(false)
                    .ConfigureKestrel(options =>
                    {
                        var port = configuration.GetValue<int>("ListenPort", 5000);
                        options.Listen(IPAddress.Any, port, listenOptions =>
                        {
                            listenOptions.Protocols = HttpProtocols.Http1AndHttp2;
                        });
                    }).UseStartup<Startup>().UseConfiguration(configuration);
           }).Build();

        private static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}
