﻿using ServerComfy.Database;
using ServerComfy.Models;

namespace ServerComfy.Extensions
{
    public static class CardDataExtension
    {
        public static Client GetClientByModel(this CardDataDto cardDataDto)
        {
            return new Client
            {
                FullName = cardDataDto.FullName,
                PhoneNumber = cardDataDto.PhoneNumber,
                BonusCard = new BonusCard
                {
                    CardEndDate = cardDataDto.EndCardDate,
                    Balance = cardDataDto.Balance
                }
            };
        }

        public static CardDataResult GetCardDataResultByClient(this Client client)
        {
            return new CardDataResult
            {
                Balance = client.BonusCard.Balance,
                FullName = client.FullName,
                PhoneNumber = client.PhoneNumber,
                EndCardDate = client.BonusCard.CardEndDate,
                CardNumber = client.BonusCard.Number
            };
        }
    }
}
