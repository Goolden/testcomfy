﻿using ServerComfy.Abstrations;
using ServerComfy.Database;
using ServerComfy.Extensions;
using ServerComfy.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ServerComfy.Services
{
    public class CardDataService : ICardDataService
    {
        private DataManager DbControl;
        readonly ComfyDbContext comfyDb;
        public CardDataService(ComfyDbContext comfyDb)
        {
            this.comfyDb = comfyDb;
            DbControl = new DataManager(this.comfyDb);
        }
        public async Task<CardDataResult> CreateUser(CardDataDto cardDataDto)
        {
            try
            {
                var client = cardDataDto.GetClientByModel();
                client.BonusCard.Number = GenerateCardNumber();

                await DbControl.CreateNewClient(client);

                return client.GetCardDataResultByClient();
            }
            catch (Exception e)
            {
                return null;
            }

        }
        private string GenerateCardNumber()
        {
           
           int result = 0;
            if (comfyDb.Clients.Count() == 0)
                result = 1;
            else
                result = comfyDb.Clients.Max(i => i.Id)+1;
           return AddStringNulls(Convert.ToString(result), 6);

        }

        private string AddStringNulls(string value, int lenght)
        {
            int nulls = lenght - value.Length;
            string resultString = "";
            if(nulls > 0)
            {
                for (int i = 0; i < nulls; i++)
                {
                    resultString += "0";
                }
            }
            return resultString + value;
        }

        public async Task<CardDataResult> FindCardData(CardFindDto cardFindDto)
        {
            try
            {
                if(cardFindDto.Data.Length == 6)
                {
                    var client = await DbControl.GetClienByCardNumberAsync(cardFindDto.Data);
                    return  client.GetCardDataResultByClient();
                }
                else
                {
                    var client = await DbControl.GetClienByPhone(cardFindDto.Data);
                    return client.GetCardDataResultByClient();
                }
            }
            catch (Exception)
            {

                return null;
            }
        }

        public async Task<CardDataResult> ChangeCardBalance(CardChangeBalanceDto cardChangeBalanceDto)
        {

            var result = await DbControl.ChangeBalance(cardChangeBalanceDto.ChangeSumm, cardChangeBalanceDto.CardNumber);

            return result.GetCardDataResultByClient();

        }
    }
}
