﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServerComfy.Database
{
    public class BonusCard
    {
        [Key]
        public string Number { get; set; }

        public DateTime CardEndDate { get; set; }

        public double Balance { get; set; }

        public string ClientRef { get; set; }

        public Client Client { get; set; }
    }
}
