﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServerComfy.Database
{
    public class Client
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Key]
        public string PhoneNumber { get; set; }

        public string FullName { get; set; }
        
        public BonusCard BonusCard { get; set; }
    }
}
