﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace ServerComfy.Database
{
    public class DataManager
    {
        private readonly ComfyDbContext comfyDb;
        public DataManager(ComfyDbContext comfyDb)
        {
            this.comfyDb = comfyDb;
        }

        public async Task CreateNewClient(Client client)
        {
            comfyDb.Clients.Add(client);
            await comfyDb.SaveChangesAsync();
        }

        public async Task<Client> GetClienByPhone(string phoneNumber)
        {
            var clients = comfyDb.Clients.Include(p => p.BonusCard);
            return await clients.FirstOrDefaultAsync(p => p.PhoneNumber == phoneNumber);
        }

        public async Task<Client> GetClienByCardNumberAsync(string cardNumber)
        {
            var clients = comfyDb.Clients.Include(p => p.BonusCard);
            var result = clients.FirstOrDefaultAsync(p => p.BonusCard.Number == cardNumber);
            return await result;
        }

        public async Task<Client> ChangeBalance(double changeSumm, string cardNumber)
        {
            var clients = comfyDb.Clients.Include(p => p.BonusCard);
            var client = await clients.FirstOrDefaultAsync(p => p.BonusCard.Number == cardNumber);
            if (client.BonusCard.CardEndDate >= DateTime.Now)
            {
                client.BonusCard.Balance += changeSumm;
                comfyDb.Clients.Update(client);
                await comfyDb.SaveChangesAsync();
                return client;
            }
            else
                return null;
        }
    }
}
