﻿using Microsoft.EntityFrameworkCore;

namespace ServerComfy.Database
{
    public class ComfyDbContext : DbContext
    {
        public ComfyDbContext(DbContextOptions<ComfyDbContext> options) : base(options)
        {
        }

        public DbSet<Client> Clients { get; set; }

        public DbSet<BonusCard> BonusCards { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Client>()


                .HasOne(c => c.BonusCard)
                .WithOne(b => b.Client)


                .HasForeignKey<BonusCard>(b => b.ClientRef);

            base.OnModelCreating(builder);


        }
    }
}
